from django.urls import path
from projects.views import (
    ProjectListView,
    ProjectCreateView,
    ProjectDetailView,
)
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path("", ProjectListView.as_view(), name="project_list"),
    path(
        "create/",
        login_required(ProjectCreateView.as_view()),
        name="project_create",
    ),
    path(
        "projects/<slug:slug>/",
        ProjectDetailView.as_view(),
        name="project_detail",
    ),
]
