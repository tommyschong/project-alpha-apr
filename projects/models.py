from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


# Create your models here.


class Project(models.Model):
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="projects"
    )
    name = models.CharField(max_length=200)
    description = models.TextField()
    start_date = models.DateField(default=timezone.now)
    end_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.name
