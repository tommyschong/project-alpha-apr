from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from projects.models import Project
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


@method_decorator(login_required, name="dispatch")
class ProjectListView(ListView):
    model = Project
    template_name = "projects/project_list.html"
    context_object_name = "project_list"


class ProjectDetailView(DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "project"
    slug_url_kwarg = "slug"


class ProjectCreateView(CreateView):
    model = Project
    fields = ["name", "description", "owner"]
    template_name = "projects/project_create.html"
    success_url = reverse_lazy("projects:project_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class ProjectUpdateView(UpdateView):
    model = Project
    template_name = "projects/project_form.html"
    fields = ["name", "description", "start_date", "end_date"]
    success_url = reverse_lazy("projects:project_list")


class ProjectDeleteView(DeleteView):
    model = Project
    template_name = "projects/project_confirm_delete.html"
    success_url = reverse_lazy("projects:project_list")
