from django.db import models
from django.contrib.auth.models import User
from projects.models import Project
from datetime import datetime
from django.utils import timezone

now = timezone.now()

# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(default=datetime.now)
    due_date = models.DateTimeField(default=datetime.now)
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="tasks", default=None
    )
    assignee = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="tasks"
    )
