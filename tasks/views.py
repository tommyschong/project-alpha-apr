from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView


from tasks.models import Task
from tasks.forms import TaskForm

# Create your views here.


@method_decorator(login_required, name="dispatch")
class CreateTaskView(CreateView):
    model = Task
    form_class = TaskForm
    template_name = "tasks/create_task.html"
    success_url = reverse_lazy("project_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


@method_decorator(login_required, name="dispatch")
class TaskListView(ListView):
    model = Task
    template_name = "my_tasks.html"


def get_queryset(self):
    if hasattr(self.request, "user") and self.request.user.is_authenticated:
        return Task.objects.filter(assignee=self.request.user)
    else:
        return Task.objects.none()
