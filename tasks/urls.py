from django.urls import path
from tasks.views import CreateTaskView, TaskListView
from django.contrib.auth.decorators import login_required


urlpatterns = [
    path(
        "create/", login_required(CreateTaskView.as_view()), name="create_task"
    ),
    path("mine/", login_required(TaskListView.as_view()), name="my_tasks"),
]
